//
//  NSDateExtension.swift
//  MisterAladin
//
//  Created by Rifat Firdaus on 3/15/16.
//  Copyright © 2016 Rifat Firdaus. All rights reserved.
//

import Foundation

extension Date {

    func dateByAddingDay(_ day:Int) -> Date {
        return (Calendar.current as NSCalendar).date(
            byAdding: .day,
            value: day,
            to: self,
            options: [])!
    }

    func daysDiff(_ date: Date) -> Int {
        let interval = self.normalizeTime().timeIntervalSince(date.normalizeTime())
        let diff = interval / (24 * 3600)
        return Int(abs(diff))
    }

    func normalizeTime() -> Date {
        let calendar = Calendar.current
        return (calendar as NSCalendar).date(bySettingHour: 0, minute: 0, second: 0, of: self, options: [])!
    }

    func stringToNSDate(_ dateString: String?) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if let dateString = dateString {
            if dateString == "0000-00-00" {
                let dateFromString = dateFormatter.date(from: "1945-01-01")
                return dateFromString!
            }
            let dateFromString = dateFormatter.date(from: dateString)
            return dateFromString!
        } else {
            let dateFromString = dateFormatter.date(from: "1945-01-01")
            return dateFromString!
        }
    }
    
    func toString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        let dateFromString = dateFormatter.string(from: self)
        return dateFromString
    }
    
    func toStringJsonFormat() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateFromString = dateFormatter.string(from: self)
        return dateFromString
    }
}
