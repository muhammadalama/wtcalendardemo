//
//  ViewController.swift
//  WtCalendarDemo
//
//  Created by Muhammad Alam Akbar on 1/12/17.
//  Copyright © 2017 Muhammad Alam Akbar. All rights reserved.
//

import UIKit
import WTCalendarView

class ViewController: UIViewController {

    @IBOutlet weak var calendarView: CalendarView!
    
    var checkInDate: Date!
    var checkOutDate: Date!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       
        checkInDate = Date().dateByAddingDay(0)
        checkOutDate = Date().dateByAddingDay(3)
        
        calendarView.localeIdentifier = "en_US"
        calendarView.delegate = self
        calendarView.setMinDate(checkInDate, maxDate: Date().dateByAddingDay(366 * 2))
        calendarView.setBeginDate(checkInDate, finishDate: checkOutDate)
        calendarView.scrollToMonthOfDate(checkInDate)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension ViewController: CalendarViewDelegate {
    
    func calendarView(_ calendarView: CalendarView, didUpdateBeginDate beginDate: Date?) {
        if let begin = beginDate {
            self.checkInDate = begin
        }
    }
    
    func calendarView(_ calendarView: CalendarView, didUpdateFinishDate finishDate: Date?) {
        if let finish = finishDate {
            self.checkOutDate = finish
        }
    }
}
