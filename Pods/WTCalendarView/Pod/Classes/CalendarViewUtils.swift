//
//  CalendarViewUtils.swift
//  CalendarView
//
//  Created by Wito Chandra on 05/04/16.
//  Copyright © 2016 Wito Chandra. All rights reserved.
//

import Foundation

class CalendarViewUtils: NSObject {

    static let instance = CalendarViewUtils()
    
    var calendar: Calendar
    let bundle: Bundle
    
    override init() {
        calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone.autoupdatingCurrent
        let url = Bundle(for: type(of: self)).url(forResource: "WTCalendarView", withExtension: ".bundle")
        bundle = Bundle(url: url!)!
        super.init()
    }
}
