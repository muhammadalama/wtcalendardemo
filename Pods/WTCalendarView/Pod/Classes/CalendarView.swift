//
//  CalendarView.swift
//  CalendarView
//
//  Created by Wito Chandra on 05/04/16.
//  Copyright © 2016 Wito Chandra. All rights reserved.
//

import UIKit

open class CalendarView: UIView {
    
    @IBOutlet fileprivate var viewBackground: UIView!
    @IBOutlet fileprivate var viewTitleContainer: UIView!
    @IBOutlet fileprivate var viewDaysOfWeekContainer: UIView!
    @IBOutlet fileprivate var viewDivider: UIView!
    @IBOutlet fileprivate var collectionView: UICollectionView!
    @IBOutlet fileprivate var labelTitle: UILabel!
    @IBOutlet fileprivate var labelSunday: UILabel!
    @IBOutlet fileprivate var labelMonday: UILabel!
    @IBOutlet fileprivate var labelTuesday: UILabel!
    @IBOutlet fileprivate var labelWednesday: UILabel!
    @IBOutlet fileprivate var labelThursday: UILabel!
    @IBOutlet fileprivate var labelFriday: UILabel!
    @IBOutlet fileprivate var labelSaturday: UILabel!
    @IBOutlet fileprivate var buttonPrevious: UIButton!
    @IBOutlet fileprivate var buttonNext: UIButton!
    
    fileprivate var gestureDragDate: UIPanGestureRecognizer!
    
    fileprivate var currentFirstDayOfMonth: Date
    fileprivate var firstDate: Date
    fileprivate var endDate: Date
    
    fileprivate var lastFrame = CGRect.zero
    fileprivate var beginIndex: Int?
    fileprivate var endIndex: Int?
    fileprivate var draggingBeginDate = false
    
    fileprivate let dateFormatter = DateFormatter()
    
    var currentIndexScroll = 0
    let daySum = (42*24)
    
    fileprivate var minDate: Date {
        didSet {
            firstDate = minDate.firstDayOfCurrentMonth().lastSunday()
        }
    }
    
    fileprivate var maxDate = Date().dateByAddingDay(365) {
        didSet {
            endDate = maxDate.endDayOfCurrentMonth().nextSaturday()
        }
    }
    
    fileprivate var lastConstantOffset = CGPoint(x: 0, y: 0)
    // MARK: - Public Properties
    
    open var beginDate: Date? {
        guard let index = beginIndex else {
            return nil
        }
        return firstDate.dateByAddingDay(index)
    }
    
    open var finishDate: Date? {
        guard let index = endIndex else {
            return nil
        }
        return firstDate.dateByAddingDay(index)
    }
    
    open var imagePreviousName = "ic_arrow_left_blue.png" {
        didSet {
            buttonPrevious.setImage(UIImage(named: imagePreviousName), for: UIControlState())
        }
    }
    
    open var imageNextName = "ic_arrow_right_blue.png" {
        didSet {
            buttonNext.setImage(UIImage(named: imageNextName), for: UIControlState())
        }
    }
    
    open var localeIdentifier: String = "en_US" {
        didSet {
            dateFormatter.locale = Locale(identifier: localeIdentifier)
            reload()
        }
    }
    
    open var delegate: CalendarViewDelegate?
    
    // MARK: - Constructors
    
    override init(frame: CGRect) {
        minDate = Date().normalizeTime()
        maxDate = minDate.dateByAddingDay(365)
        firstDate = minDate.firstDayOfCurrentMonth().lastSunday()
        endDate = maxDate.endDayOfCurrentMonth().dateByAddingDay(7).nextSaturday()
        currentFirstDayOfMonth = minDate.firstDayOfCurrentMonth()
        
        super.init(frame: frame)
        loadViews()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        minDate = Date().normalizeTime()
        maxDate = minDate.dateByAddingDay(365)
        firstDate = minDate.firstDayOfCurrentMonth().lastSunday()
        endDate = maxDate.endDayOfCurrentMonth().dateByAddingDay(7).nextSaturday()
        currentFirstDayOfMonth = minDate.firstDayOfCurrentMonth()
        
        super.init(coder: aDecoder)
        loadViews()
    }
    
    // MARK: - Overrides
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        
        if !lastFrame.equalTo(frame) {
            lastFrame = frame
            collectionView?.reloadData()
        }
        
        collectionView.layoutIfNeeded()
        scrollToMonthOfDate(currentFirstDayOfMonth)
    }
    
    // MARK: - Methods
    
    fileprivate func loadViews() {
        let view = CalendarViewUtils.instance.bundle.loadNibNamed("CalendarView", owner: self, options: nil)?.first as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    
        gestureDragDate = UIPanGestureRecognizer(target: self, action: #selector(CalendarView.handleDragDate(_:)))
        gestureDragDate.delegate = self
        
        collectionView.addGestureRecognizer(gestureDragDate)
        
        collectionView.register(UINib(nibName: "CalendarDayCell", bundle: CalendarViewUtils.instance.bundle), forCellWithReuseIdentifier: "DayCell")
        
        reload()
    }
    
    func convertIndex(_ row: Int) -> Int {
        let i = row
        let a = Int(floor(Double(i%42)/6.0))
        let b = Int(((i%6)*7))
        let c = Int(floor(floor(Double(i)/6.0)/7.0)*42)
        let i2 = a+b+c
        return i2
    }
    
    open func scrollToMonthOfDate(_ date: Date) {
        if date.compare(maxDate.firstDayOfCurrentMonth()) != .orderedDescending && date.compare(firstDate) != .orderedAscending {
            currentFirstDayOfMonth = date.firstDayOfCurrentMonth()
            var calendar = CalendarViewUtils.instance.calendar
            calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
            let diff = (calendar as NSCalendar).components(.day, from: firstDate, to: currentFirstDayOfMonth, options: [])
            //let newRowDay = self.convertIndex(diff.day!)
            //collectionView.scrollToItem(at: IndexPath(row: diff.day!, section: 0), at: .left, animated: true)
            print(diff.day);
        }
        let i = currentIndexScroll * 42
        collectionView.scrollToItem(at: IndexPath(row: i, section: 0), at: .left, animated: true)
        collectionView.reloadData()
        updateMonthYearViews()
    }
    
    open func setMinDate(_ minDate: Date, maxDate: Date) {
        if minDate.compare(maxDate) != .orderedAscending {
            fatalError("Min date must be earlier than max date")
        }
        self.minDate = minDate.normalizeTime()
        self.maxDate = maxDate.normalizeTime()
        
        // scrollToMonthOfDate(minDate)
        collectionView.reloadData()
    }
    
    open func setBeginDate(_ beginDate: Date?, finishDate: Date?) {
        if beginDate == nil {
            beginIndex = nil
            endIndex = nil
            collectionView.reloadData()
            return
        }
        if let beginDate = beginDate {
            var calendar = CalendarViewUtils.instance.calendar
            calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
            let components = (calendar as NSCalendar).components(.day, from: firstDate, to: beginDate, options: [])
            if beginDate.compare(minDate) != .orderedAscending {
                beginIndex = components.day
            } else {
                beginIndex = nil
            }
            if let finishDate = finishDate {
                let components = (calendar as NSCalendar).components(.day, from: firstDate, to: finishDate, options: [])
                if finishDate.compare(maxDate) != .orderedDescending {
                    endIndex = components.day
                } else {
                    endIndex = nil
                }
            } else {
                endIndex = nil
            }
            collectionView.reloadData()
        }
    }
    
    open func reload() {
        viewTitleContainer.backgroundColor = CalendarViewTheme.instance.bgColorForMonthContainer
        viewDaysOfWeekContainer.backgroundColor = CalendarViewTheme.instance.bgColorForDaysOfWeekContainer
        viewBackground.backgroundColor = CalendarViewTheme.instance.bgColorForOtherMonth
        labelSunday.textColor = CalendarViewTheme.instance.textColorForDayOfWeek
        labelMonday.textColor = CalendarViewTheme.instance.textColorForDayOfWeek
        labelTuesday.textColor = CalendarViewTheme.instance.textColorForDayOfWeek
        labelWednesday.textColor = CalendarViewTheme.instance.textColorForDayOfWeek
        labelThursday.textColor = CalendarViewTheme.instance.textColorForDayOfWeek
        labelFriday.textColor = CalendarViewTheme.instance.textColorForDayOfWeek
        labelSaturday.textColor = CalendarViewTheme.instance.textColorForDayOfWeek
        viewDivider.backgroundColor = CalendarViewTheme.instance.colorForDivider
        
        labelTitle.textColor = CalendarViewTheme.instance.textColorForTitle
        
        let date = minDate.lastSunday()
        dateFormatter.dateFormat = "EEEEE"
        labelSunday.text = dateFormatter.string(from: date)
        labelMonday.text = dateFormatter.string(from: date.dateByAddingDay(1))
        labelTuesday.text = dateFormatter.string(from: date.dateByAddingDay(2))
        labelWednesday.text = dateFormatter.string(from: date.dateByAddingDay(3))
        labelThursday.text = dateFormatter.string(from: date.dateByAddingDay(4))
        labelFriday.text = dateFormatter.string(from: date.dateByAddingDay(5))
        labelSaturday.text = dateFormatter.string(from: date.dateByAddingDay(6))
        
        updateMonthYearViews()
        
        collectionView.reloadData()
    }
}

// MARK: - Month View

extension CalendarView {
    
    @IBAction fileprivate func buttonPreviousMonthPressed() {
        scrollToPreviousMonth()
    }
    
    @IBAction fileprivate func buttonNextMonthPressed() {
        scrollToNextMonth()
    }
    
    fileprivate func updateMonthYearViews() {
        dateFormatter.dateFormat = "MMMM yyyy"
        labelTitle.text = dateFormatter.string(from: currentFirstDayOfMonth)
    }
    
    fileprivate func scrollToPreviousMonth() {
        currentIndexScroll = currentIndexScroll - 1
        if currentIndexScroll < 0 {
            currentIndexScroll = 0
        }
        
        var calendar = CalendarViewUtils.instance.calendar
        calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
        var diffComponents = DateComponents()
        diffComponents.month = -1
        let date = (calendar as NSCalendar).date(byAdding: diffComponents, to: currentFirstDayOfMonth, options: [])
        if let date = date {
            scrollToMonthOfDate(date)
        }
    }
    
    fileprivate func scrollToNextMonth() {
        currentIndexScroll = currentIndexScroll + 1
        if currentIndexScroll > 23 {
            currentIndexScroll = 23
        }
        
        var calendar = CalendarViewUtils.instance.calendar
        calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
        var diffComponents = DateComponents()
        diffComponents.month = 1
        let date = (calendar as NSCalendar).date(byAdding: diffComponents, to: currentFirstDayOfMonth, options: [])
        if let date = date {
            scrollToMonthOfDate(date)
        }
    }
    
    func getDiffDay(_ dateFrom:Date, _ dateTo:Date) -> Int {
        var calendar = CalendarViewUtils.instance.calendar
        calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
        let diff = (calendar as NSCalendar).components(.day, from: dateFrom, to: dateTo, options: [])
        return diff.day!
    }
}

// MARK: - UICollectionView

extension CalendarView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //let calendar = CalendarViewUtils.instance.calendar
        //calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
        //let components = (calendar as NSCalendar).components(.day, from: firstDate, to: endDate, options: [])
        return daySum
    }
    
    func getDayOfWeek(_ todayDate:Date) -> Int? {
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
    }
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var newRow = convertIndex(indexPath.row)
        
        let sectionMonth = Int(floor(Double(newRow)/42.0))
        let a = minDate.firstDayOfCurrentMonth().dateByAddingMonth(sectionMonth).lastSunday()
        let diff_a_firstDate = getDiffDay(firstDate, a)
        var diffDay = (sectionMonth*42) - diff_a_firstDate
        
        var row2 = newRow - diffDay
    
        let date = firstDate.dateByAddingDay(row2)
        let disabled = date.normalizeTime().compare(minDate) == ComparisonResult.orderedAscending
        let state: CalendarDayCellState
        
        if disabled {
            state = .disabled
        } else if let beginIndex = beginIndex, beginIndex == (newRow-diffDay) {
            print("xx date : \(date)")
            print("xx beginIndex : \(beginIndex)")
            print("xx newrow : \(newRow)")
            state = .start(hasNext: endIndex != nil)
        } else if let endIndex = endIndex, endIndex == (newRow-diffDay) {
            state = .end
        } else if let beginIndex = beginIndex, let endIndex = endIndex, (newRow-diffDay) > beginIndex && (newRow-diffDay) < endIndex {
            state = .range
        } else {
            state = .normal
        }
        
        var calendar = CalendarViewUtils.instance.calendar
        calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
        let components = (calendar as NSCalendar).components([.month, .year], from: date)
        let currentComponents = (calendar as NSCalendar).components([.month, .year], from: currentFirstDayOfMonth)
        let isCurrentMonth = components.month == currentComponents.month && components.year == currentComponents.year
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DayCell", for: indexPath) as! CalendarDayCell
        cell.updateWithDate(date, state: state, isCurrentMonth: isCurrentMonth)
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = widthForCellAtIndexPath(indexPath)
        return CGSize(width: width, height: 38)
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! CalendarDayCell
        let date = cell.date
        print("selected : \(date)")
        switch cell.state {
        case .disabled:
            return
        default:
            break
        }
        
        let newRow = convertIndex(indexPath.row)
        
        let sectionMonth = Int(floor(Double(newRow)/42.0))
        let a = minDate.firstDayOfCurrentMonth().dateByAddingMonth(sectionMonth).lastSunday()
        let diff_a_firstDate = getDiffDay(firstDate, a)
        var diffDay = (sectionMonth*42) - diff_a_firstDate
        
        if beginIndex == nil {
            beginIndex = (newRow-diffDay)
            delegate?.calendarView(self, didUpdateBeginDate: date)
        } else if let beginIndex = beginIndex, (newRow-diffDay) <= beginIndex && endIndex == nil {
            self.beginIndex = (newRow-diffDay)
            delegate?.calendarView(self, didUpdateBeginDate: date)
        } else if let beginIndex = beginIndex, (newRow-diffDay) > beginIndex && endIndex == nil {
            endIndex = (newRow-diffDay)
            delegate?.calendarView(self, didUpdateFinishDate: date)
            var calendar = CalendarViewUtils.instance.calendar
            calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
            let components = (calendar as NSCalendar).components([.month, .year], from: date as Date)
            let currentComponents = (calendar as NSCalendar).components([.month, .year], from: currentFirstDayOfMonth)
            if components.month != currentComponents.month {
                scrollToNextMonth()
            }
        } else if beginIndex != nil && endIndex != nil {
            beginIndex = (newRow-diffDay)
            endIndex = nil
            delegate?.calendarView(self, didUpdateBeginDate: date)
            delegate?.calendarView(self, didUpdateFinishDate: nil)
        }
        
        collectionView.reloadData()
    }
    
    public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        lastConstantOffset = scrollView.contentOffset
    }
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if lastConstantOffset.x < scrollView.contentOffset.x {
            self.scrollToNextMonth()
        }
        else if lastConstantOffset.x > scrollView.contentOffset.x {
            self.scrollToPreviousMonth()
        }
    }
    
    fileprivate func widthForCellAtIndexPath(_ indexPath: IndexPath) -> CGFloat {
        let width = bounds.width
        var cellWidth = floor(width / 7)
        let newRow = convertIndex(indexPath.row)
        if newRow % 7 == 6 {
            cellWidth = cellWidth + (width - (cellWidth * 7))
        }
        return cellWidth
    }
}

extension CalendarView: UIGestureRecognizerDelegate {
    
    open override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let point = gestureRecognizer.location(in: collectionView)
        if let indexPath = collectionView.indexPathForItem(at: point),
            let beginIndex = beginIndex,
            let endIndex = endIndex, indexPath.row == beginIndex || indexPath.row == endIndex
        {
            draggingBeginDate = indexPath.row == beginIndex
            return true
        }
        return false
    }
    
    public func handleDragDate(_ gestureRecognizer: UIGestureRecognizer) {
        let point = gestureRecognizer.location(in: collectionView)
        if let indexPath = collectionView.indexPathForItem(at: point),
                let cell = collectionView.cellForItem(at: indexPath) as? CalendarDayCell,
                let beginIndex = beginIndex,
                let endIndex = endIndex
        {
            switch cell.state {
            case .disabled:
                return
            default:
                break
            }
            let index = indexPath.row
            if draggingBeginDate {
                if index < endIndex {
                    self.beginIndex = index
                } else if index > endIndex {
                    draggingBeginDate = false
                    self.beginIndex = endIndex
                    self.endIndex = index
                }
            } else {
                if index > beginIndex {
                    self.endIndex = index
                } else if index < beginIndex {
                    draggingBeginDate = true
                    self.beginIndex = index
                    self.endIndex = beginIndex
                }
            }
            if self.beginIndex != beginIndex || self.endIndex != endIndex {
                if let index = self.beginIndex, index != beginIndex {
                    delegate?.calendarView(self, didUpdateBeginDate: firstDate.dateByAddingDay(index))
                }
                if let index = self.endIndex, index != endIndex {
                    delegate?.calendarView(self, didUpdateFinishDate: firstDate.dateByAddingDay(index))
                }
                collectionView.reloadData()
            }
        }
    }
}
