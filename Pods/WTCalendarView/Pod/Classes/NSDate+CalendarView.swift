//
//  NSDate+CalendarView.swift
//  CalendarView
//
//  Created by Wito Chandra on 05/04/16.
//  Copyright © 2016 Wito Chandra. All rights reserved.
//

import Foundation

extension Date {
    
    func dateByAddingDay(_ day: Int) -> Date {
        var calendar = CalendarViewUtils.instance.calendar
        calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
        var diff = DateComponents()
        diff.day = day
        return (calendar as NSCalendar).date(byAdding: diff, to: self, options: [])!
    }
    
    func dateByAddingMonth(_ month: Int) -> Date {
        var calendar = CalendarViewUtils.instance.calendar
        calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
        var diff = DateComponents()
        diff.month = month
        return (calendar as NSCalendar).date(byAdding: diff, to: self, options: [])!
    }

    
    func lastSunday() -> Date {
        var calendar = CalendarViewUtils.instance.calendar
        calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
        let components = (calendar as NSCalendar).components(.weekday, from: self)
        return dateByAddingDay(1 - components.weekday!)
    }
    
    func nextSaturday() -> Date {
        var calendar = CalendarViewUtils.instance.calendar
        calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
        let components = (calendar as NSCalendar).components(.weekday, from: self)
        return dateByAddingDay(7 - components.weekday!)
    }
    
    func firstDayOfCurrentMonth() -> Date {
        var calendar = CalendarViewUtils.instance.calendar
        calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
        let components = (calendar as NSCalendar).components(.day, from: self)
        return dateByAddingDay(1 - components.day!)
    }
    
    func endDayOfCurrentMonth() -> Date {
        var calendar = CalendarViewUtils.instance.calendar
        calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
        var diff = DateComponents()
        diff.month = 1
        return (calendar as NSCalendar).date(byAdding: diff, to: self, options: [])!
            .firstDayOfCurrentMonth()
            .dateByAddingDay(-1)
    }
    
    func normalizeTime() -> Date {
        var calendar = CalendarViewUtils.instance.calendar
        calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
        return (calendar as NSCalendar).date(bySettingHour: 0, minute: 0, second: 0, of: self, options: [])!
    }
    
    func realFirstDayOfCurrentMonth() -> Date {
        var calendar = NSCalendar.current
        calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
        var startDate : NSDate?
        (calendar as NSCalendar).range(of: .month, start: &startDate, interval: nil, for: self)
        return startDate! as Date
    }
}
